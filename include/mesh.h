#include "stickmaths.h"

#ifndef _MESH_H
#define _MESH_H
typedef struct Mesh
{
    Vertex* verts;
    IndexedTriangle* tris;
    size_t vcount, tricount;
} Mesh;
#endif

extern Mesh cube;
extern Mesh suzanne;

void CreateCube();
void KillCube();
void CreateSuzanne();
void KillSuzanne();