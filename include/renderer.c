#include "renderer.h"
#include "graphics.h"

Mat4 pmat;


void DrawMesh(Mesh* mesh, Mat4* tmat)
{
    //printf("cum1\n");
    Vertex* tverts = malloc(mesh->vcount*sizeof(Vertex));
    //printf("cum2\n");
    //printf("%i,%i\n",mesh->vcount,mesh->tricount);
    for(int i=0;i<mesh->vcount;i++)
    {
        TransformVector(tmat,&mesh->verts[i].p,&tverts[i].p);
        //printf("%i: %f,%f,%f\n",i,tverts[i].x,tverts[i].y,tverts[i].z);
    }
    //printf("cum3\n");

    for(int i=0;i<mesh->tricount;i++)
    {
        //printf("%i: %i, %i, %i\n",i,mesh->tris[i].v1,mesh->tris[i].v2,mesh->tris[i].v3);
        Triangle t;
        t.v1 = tverts[mesh->tris[i].v1];
        t.v2 = tverts[mesh->tris[i].v2];
        t.v3 = tverts[mesh->tris[i].v3];
        Vec3 normal;
        Vec4 e1,e2;
        Vec3Sub(&t.v2.p,&t.v1.p,&e1);
        Vec3Sub(&t.v3.p,&t.v1.p,&e2);
        e1.w = 1.0f; e2.w = 1.0f;
        Vec3CrossProduct(&e1,&e2,&normal);
        Vec3Normalize(&normal);
        if(Vec3DotProduct(&normal,&t.v1.p)<0.0f)
        {

            const float tanfov = tanf(gfov/2.0f);
            const float oneovz1 = 1.0f/(t.v1.p.z*tanfov);
            const float oneovz2 = 1.0f/(t.v2.p.z*tanfov);
            const float oneovz3 = 1.0f/(t.v3.p.z*tanfov);
            t.v1.p.x *= oneovz1; t.v1.p.y *= oneovz1; t.v1.p.z *= oneovz1; t.v1.p.w *= oneovz1;
            t.v2.p.x *= oneovz2; t.v2.p.y *= oneovz2; t.v2.p.z *= oneovz2; t.v2.p.w *= oneovz2;
            t.v3.p.x *= oneovz3; t.v3.p.y *= oneovz3; t.v3.p.z *= oneovz3; t.v3.p.w *= oneovz3;
            const float ar = (float)SCREENHEIGHT/(float)SCREENWIDTH;
            t.v1.p.x = (t.v1.p.x*ar + 1.0f)*0.5f*SCREENWIDTH; t.v1.p.y = (-t.v1.p.y + 1.0f)*0.5f*SCREENHEIGHT;
            t.v2.p.x = (t.v2.p.x*ar + 1.0f)*0.5f*SCREENWIDTH; t.v2.p.y = (-t.v2.p.y + 1.0f)*0.5f*SCREENHEIGHT;
            t.v3.p.x = (t.v3.p.x*ar + 1.0f)*0.5f*SCREENWIDTH; t.v3.p.y = (-t.v3.p.y + 1.0f)*0.5f*SCREENHEIGHT;
            //printf("%i: %f %f %f\n",i,normal.x,normal.y,normal.z);
            t.p = mesh->tris[i].p;
            t.p.n = normal;
            FillTriangle(t);
        }
    }
    free(tverts);
}

void DrawTriangle(Triangle t)
{
    DrawLine(t.v1.p.x,t.v1.p.y,t.v2.p.x,t.v2.p.y,t.p.c);
    DrawLine(t.v2.p.x,t.v2.p.y,t.v3.p.x,t.v3.p.y,t.p.c);
    DrawLine(t.v3.p.x,t.v3.p.y,t.v1.p.x,t.v1.p.y,t.p.c);
}

void FillTriangle(Triangle t)
{
    if(t.v1.p.y > t.v2.p.y)
        VertexSwap(&t.v1,&t.v2);
    if(t.v1.p.y > t.v3.p.y)
        VertexSwap(&t.v1,&t.v3);
    if(t.v2.p.y > t.v3.p.y)
        VertexSwap(&t.v2,&t.v3);

    int bend = (t.v2.p.y - t.v1.p.y) * (t.v3.p.x - t.v1.p.x) < (t.v2.p.x - t.v1.p.x) * (t.v3.p.y - t.v1.p.y);

    float dx1,dx2;
    float dz1,dz2;
    int step;

    float x1 = t.v1.p.x,x2 = t.v1.p.x,y = t.v1.p.y;
    float z1 = t.v1.p.w,z2 = t.v1.p.w;

    float light = fmaxf(0.1f,Vec3DotProduct(&t.p.n,&(Vec3){0,0,-1.0f}));

    t.p.c.r *= light;
    t.p.c.g *= light;
    t.p.c.b *= light;


    if((int)t.v1.p.y != (int)t.v2.p.y)
    {

        if(bend == 0)
        {
            dx1 = (t.v2.p.x - t.v1.p.x) / (t.v2.p.y - t.v1.p.y);
            dx2 = (t.v3.p.x - t.v1.p.x) / (t.v3.p.y - t.v1.p.y);
            dz1 = (t.v2.p.w - t.v1.p.w) / (t.v2.p.y - t.v1.p.y);
            dz2 = (t.v3.p.w - t.v1.p.w) / (t.v3.p.y - t.v1.p.y);
        }
        else
        {
            dx1 = (t.v3.p.x - t.v1.p.x) / (t.v3.p.y - t.v1.p.y);
            dx2 = (t.v2.p.x - t.v1.p.x) / (t.v2.p.y - t.v1.p.y);
            dz1 = (t.v3.p.w - t.v1.p.w) / (t.v3.p.y - t.v1.p.y);
            dz2 = (t.v2.p.w - t.v1.p.w) / (t.v2.p.y - t.v1.p.y);
        }
        while(y<t.v2.p.y)
        {
            float dz3 = (z2 - z1) / (x2 - x1);
            for(float x = x1, z = z1; x < x2; x++,z+=dz3)
            {
                PutPixelZ((int)floorf(x),(int)floorf(y),z,t.p.c);
            }
            x1 += dx1;
            x2 += dx2;
            z1 += dz1;
            z2 += dz2;
            y++;
        }
    }
    
    if((int)t.v2.p.y != (int)t.v3.p.y)
    {
        if(bend == 0)
        {
            dx1 = (t.v3.p.x - t.v2.p.x) / (t.v3.p.y - t.v2.p.y);
            dx2 = (t.v3.p.x - t.v1.p.x) / (t.v3.p.y - t.v1.p.y);
            dz1 = (t.v3.p.w - t.v2.p.w) / (t.v3.p.y - t.v2.p.y);
            dz2 = (t.v3.p.w - t.v1.p.w) / (t.v3.p.y - t.v1.p.y);
            x1 = t.v2.p.x;
            z1 = t.v2.p.w;
        }
        else
        {
            dx1 = (t.v3.p.x - t.v1.p.x) / (t.v3.p.y - t.v1.p.y);
            dx2 = (t.v3.p.x - t.v2.p.x) / (t.v3.p.y - t.v2.p.y);
            dz1 = (t.v3.p.w - t.v1.p.w) / (t.v3.p.y - t.v1.p.y);
            dz2 = (t.v3.p.w - t.v2.p.w) / (t.v3.p.y - t.v2.p.y);
            x2 = t.v2.p.x;
            z2 = t.v2.p.w;
        }

        
        while(y<t.v3.p.y)
        {
            float dz3 = (z2 - z1) / (x2 - x1);
            for(float x = x1, z = z1; x < x2; x++,z+=dz3)
            {
                PutPixelZ((int)floorf(x),(int)floorf(y),z,t.p.c);
            }
            x1 += dx1;
            x2 += dx2;
            z1 += dz1;
            z2 += dz2;
            y++;
        }
    }
}
