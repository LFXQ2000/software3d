#include <math.h>
#include <SDL2/SDL.h>

#define PI 3.14159265359f

#define DegToRad(deg) (deg/(PI*2.0f))

#ifndef _STICKMATHS_H
#define _STICKMATHS_H
typedef struct Vec3
{
    float x,y,z;
} Vec3;

typedef struct Vec4
{
    float x,y,z,w;
} Vec4;

typedef struct Mat4
{
    float m[4][4];
} Mat4;

typedef struct Vertex
{
    Vec4 p;
} Vertex;

typedef struct TriangleProps
{
    SDL_Color c;
    Vec3 n;
} TriangleProps;

typedef struct Triangle
{
    Vertex v1,v2,v3;
    TriangleProps p;
} Triangle;

typedef struct IndexedTriangle
{
    size_t v1,v2,v3;
    TriangleProps p;
} IndexedTriangle;
#endif

void Vec3Normalize(Vec3* v);
float Vec3DotProduct(Vec3* a, Vec3* b);
void Vec3CrossProduct(Vec3* a, Vec3* b, Vec3* out);
void Vec3Add(Vec3* a, Vec3* b, Vec3* out);
void Vec3Sub(Vec3* a, Vec3* b, Vec3* out);
void Vec3Swap(Vec3* a, Vec3* b);
void Vec4Swap(Vec4* a, Vec4* b);
void VertexSwap(Vertex* a, Vertex* b);

void MatrixPerspective(float ar, float fov, float far, float near, Mat4* out);

void MatrixIdentity(Mat4* out);
void MatrixTranslate(Vec3* v, Mat4* out);
void MatrixScale(Vec3* v, Mat4* out);
void MatrixRotateX(float theta, Mat4* out);
void MatrixRotateY(float theta, Mat4* out);
void MatrixRotateZ(float theta, Mat4* out);

void MatrixMultiply(Mat4* a, Mat4* b, Mat4* out);

void TransformVector(Mat4* m, Vec4* v, Vec4* out);