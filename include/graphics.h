#include <SDL2/SDL.h>

#if(SDL_BYTEORDER == SDL_BIG_ENDIAN)
	#define PIXEL_MASK_Red    0xff000000
	#define PIXEL_MASK_Green  0x00ff0000
	#define PIXEL_MASK_Blue   0x0000ff00
	#define PIXEL_MASK_Alpha  0x000000ff
#else
	#define PIXEL_MASK_Red    0x000000ff
	#define PIXEL_MASK_Green  0x0000ff00
	#define PIXEL_MASK_Blue   0x00ff0000
	#define PIXEL_MASK_Alpha  0xff000000
#endif

#define COLOR_32_BIT 32

#define SCREENWIDTH 1280
#define SCREENHEIGHT 720

extern float gfov;

void InitSDL();
void ClearRender(SDL_Color c);
void PresentRender();


void PutPixel(int x, int y, SDL_Color c);
void PutPixelZ(int x, int y, float z, SDL_Color c);
void DrawLine(float x1, float y1, float x2, float y2, SDL_Color c);
