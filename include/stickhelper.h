static inline void SwapF(float* a, float* b)
{
    float temp = *a;
    *a = *b;
    *b = temp;
}