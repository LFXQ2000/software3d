#include <SDL2/SDL.h>

enum
{
    KEY_RELEASED = 0,
    KEY_HELD = 1,
    KEY_PRESSED = 2
};

struct Keys {
    int q;
    int w;
    int e;
    int a;
    int s;
    int d;
    int left;
    int right;
    int up;
    int down;
    int plus;
    int minus;
    int leftb;
    int rightb;
};

extern struct Keys keys;

int HandleEvents();