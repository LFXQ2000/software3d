#include <SDL2/SDL.h>
#include "mesh.h"

extern Mat4 pmat;

void DrawMesh(Mesh* mesh, Mat4* tmat);
void DrawTriangle(Triangle t);
void FillTriangle(Triangle t);