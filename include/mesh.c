#include "mesh.h"
#include "fast_obj.h"

Mesh cube;

void CreateCube()
{
    cube.verts = malloc(8*sizeof(Vec4));

    cube.verts[cube.vcount++] = (Vertex){(Vec4){-1.0f,1.0f,-1.0f,1.0f   }}; //0
    cube.verts[cube.vcount++] = (Vertex){(Vec4){1.0f,1.0f,-1.0f,1.0f    }}; //1
    cube.verts[cube.vcount++] = (Vertex){(Vec4){1.0f,-1.0f,-1.0f,1.0f   }}; //2
    cube.verts[cube.vcount++] = (Vertex){(Vec4){-1.0f,-1.0f,-1.0f,1.0f  }}; //3
    cube.verts[cube.vcount++] = (Vertex){(Vec4){-1.0f,1.0f,1.0f,1.0f    }}; //4
    cube.verts[cube.vcount++] = (Vertex){(Vec4){1.0f,1.0f,1.0f,1.0f     }}; //5
    cube.verts[cube.vcount++] = (Vertex){(Vec4){1.0f,-1.0f,1.0f,1.0f    }}; //6
    cube.verts[cube.vcount++] = (Vertex){(Vec4){-1.0f,-1.0f,1.0f,1.0f   }}; //7

    cube.tris = malloc(12*sizeof(IndexedTriangle));

    //front face
    cube.tris[cube.tricount++] = (IndexedTriangle){0,1,2,(SDL_Color){255,255,255,255}};
    cube.tris[cube.tricount++] = (IndexedTriangle){0,2,3,(SDL_Color){255,255,255,255}};

    //back face
    cube.tris[cube.tricount++] = (IndexedTriangle){5,4,7,(SDL_Color){255,255,255,255}};
    cube.tris[cube.tricount++] = (IndexedTriangle){5,7,6,(SDL_Color){255,255,255,255}};

    //left face
    cube.tris[cube.tricount++] = (IndexedTriangle){4,0,3,(SDL_Color){255,255,255,255}};
    cube.tris[cube.tricount++] = (IndexedTriangle){4,3,7,(SDL_Color){255,255,255,255}};

    //right face
    cube.tris[cube.tricount++] = (IndexedTriangle){1,5,6,(SDL_Color){255,255,255,255}};
    cube.tris[cube.tricount++] = (IndexedTriangle){1,6,2,(SDL_Color){255,255,255,255}};

    //top face
    cube.tris[cube.tricount++] = (IndexedTriangle){4,5,1,(SDL_Color){255,255,255,255}};
    cube.tris[cube.tricount++] = (IndexedTriangle){4,1,0,(SDL_Color){255,255,255,255}};

    //bottom face
    cube.tris[cube.tricount++] = (IndexedTriangle){3,2,6,(SDL_Color){255,255,255,255}};
    cube.tris[cube.tricount++] = (IndexedTriangle){3,6,7,(SDL_Color){255,255,255,255}};

    printf("%i,%i\n",cube.vcount,cube.tricount);
}

void KillCube()
{
    free(cube.tris);
    free(cube.verts);
}

Mesh suzanne;

void CreateSuzanne()
{
    fastObjMesh* fomsuz;
    fomsuz = fast_obj_read("models/suzanne.obj");
    const unsigned int pcount = fomsuz->position_count+1;
    
    suzanne.verts = malloc(pcount*sizeof(Vertex));

    for(int i=1;i<pcount;i++)
    {
        float* p = fomsuz->positions+i*3;
        suzanne.verts[suzanne.vcount++] = (Vertex){(Vec4){p[0],p[1],p[2],1.0f}};
    }

    const unsigned int icount = (fomsuz->index_count/3);
    suzanne.tris = malloc((icount)*sizeof(IndexedTriangle));

    for(int i=0;i<icount;i++)
    {
        fastObjIndex* f = fomsuz->indices+i*3;
        suzanne.tris[suzanne.tricount++] = (IndexedTriangle){f[0].p-1,f[1].p-1,f[2].p-1,(SDL_Color){255,255,255,255}};
    }
    fast_obj_destroy(fomsuz);
}

void KillSuzanne()
{
    free(suzanne.tris);
    free(suzanne.verts);
}