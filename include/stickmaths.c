#include "stickmaths.h"

void Vec3Normalize(Vec3* v)
{
    float w = sqrtf(v->x*v->x + v->y*v->y + v->z*v->z);
    v->x /= w;
    v->y /= w;
    v->z /= w;
}

float Vec3DotProduct(Vec3* a, Vec3* b)
{
    return a->x*b->x + a->y*b->y + a->z*b->z;
}

void Vec3CrossProduct(Vec3* a, Vec3* b, Vec3* out)
{
    out->x = a->y*b->z - a->z*b->y;
    out->y = a->z*b->x - a->x*b->z;
    out->z = a->x*b->y - a->y*b->x;
}
void Vec3Add(Vec3* a, Vec3* b, Vec3* out)
{
    out->x = a->x + b->x;
    out->y = a->y + b->y;
    out->z = a->z + b->z;
}
void Vec3Sub(Vec3* a, Vec3* b, Vec3* out)
{
    out->x = a->x - b->x;
    out->y = a->y - b->y;
    out->z = a->z - b->z;
}
void Vec3Swap(Vec3* a, Vec3* b)
{
    Vec3 temp = *a;
    *a = *b;
    *b = temp;
}
void Vec4Swap(Vec4* a, Vec4* b)
{
    Vec4 temp = *a;
    *a = *b;
    *b = temp;
}

void VertexSwap(Vertex* a, Vertex* b)
{
    Vertex temp = *a;
    *a = *b;
    *b = temp;
}

void MatrixMultiply(Mat4* a, Mat4* b, Mat4* out)
{
    for(int i=0;i<4;i++)
    {
        for(int j=0;j<4;j++)
        {
            float result = 0;
            for(int k=0;k<4;k++)
            {
                result += a->m[i][k] * b->m[k][j];
            }
            out->m[i][j] = result;
        }
    }
}

void TransformVector(Mat4* m, Vec4* v, Vec4* out)
{
    out->x = v->x*m->m[0][0] + v->y*m->m[1][0] + v->z*m->m[2][0] + v->w*m->m[3][0];
    out->y = v->x*m->m[0][1] + v->y*m->m[1][1] + v->z*m->m[2][1] + v->w*m->m[3][1];
    out->z = v->x*m->m[0][2] + v->y*m->m[1][2] + v->z*m->m[2][2] + v->w*m->m[3][2];
    out->w = v->x*m->m[0][3] + v->y*m->m[1][3] + v->z*m->m[2][3] + v->w*m->m[3][3];
}


void MatrixPerspective(float ar, float fov, float far, float near, Mat4* out)
{
    Mat4 m;
    const float artan = tanf(fov/2.0f);
    m.m[0][0] = 1.0f/(ar*artan);
    m.m[1][1] = 1.0f/(artan);
    m.m[2][2] = (-near-far)/(near-far);
    m.m[2][3] = (2*far*near)/(near-far);
    m.m[3][2] = 1.0f;
}


void MatrixIdentity(Mat4* out)
{
    Mat4 m;
    m.m[0][0] = 1.0f;
    m.m[1][1] = 1.0f;
    m.m[2][2] = 1.0f;
    m.m[3][3] = 1.0f;
    *out = m;
}

void MatrixTranslate(Vec3* v, Mat4* out)
{
    MatrixIdentity(out);
    out->m[3][0] = v->x;
    out->m[3][1] = v->y;
    out->m[3][2] = v->z;
}

void MatrixScale(Vec3* v, Mat4* out)
{
    MatrixIdentity(out);
    out->m[0][0] = v->x;
    out->m[1][1] = v->y;
    out->m[2][2] = v->z;
}

void MatrixRotateX(float theta, Mat4* out)
{
    MatrixIdentity(out);
    out->m[1][1] = cosf(theta); out->m[2][1] = sinf(theta);
    out->m[1][2] = -sinf(theta); out->m[2][2] = cosf(theta);
}

void MatrixRotateY(float theta, Mat4* out)
{
    MatrixIdentity(out);
    out->m[0][0] = cosf(theta); out->m[2][0] = -sinf(theta);
    out->m[0][2] = sinf(theta); out->m[2][2] = cosf(theta);
}

void MatrixRotateZ(float theta, Mat4* out)
{
    MatrixIdentity(out);
    out->m[0][0] = cosf(theta); out->m[1][0] = sinf(theta);
    out->m[0][1] = -sinf(theta); out->m[1][1] = cosf(theta);
}