#include "input.h"

struct Keys keys ={

};

#define KeyDown(keycode,key) case keycode: keys.key = KEY_PRESSED|KEY_HELD; break;
#define KeyUp(keycode,key) case keycode: keys.key = KEY_RELEASED; break;

void HandleKeyPress(int* key)
{
    if(*key & (KEY_PRESSED|KEY_HELD))
        *key = KEY_HELD;
}

int HandleEvents()
{
    SDL_Event e;
    HandleKeyPress(&keys.q);
    HandleKeyPress(&keys.w);
    HandleKeyPress(&keys.e);
    HandleKeyPress(&keys.a);
    HandleKeyPress(&keys.s);
    HandleKeyPress(&keys.d);
    HandleKeyPress(&keys.left);
    HandleKeyPress(&keys.right);
    HandleKeyPress(&keys.up);
    HandleKeyPress(&keys.down);
    HandleKeyPress(&keys.plus);
    HandleKeyPress(&keys.minus);
    HandleKeyPress(&keys.leftb);
    HandleKeyPress(&keys.rightb);
    int retcode = 0;
    while(SDL_PollEvent(&e)!=0)
    {
        switch(e.type)
        {
            case SDL_QUIT:
                retcode = 1;
                break;
            case SDL_KEYDOWN:
                if(e.key.repeat == 0)
                {
                    switch(e.key.keysym.sym)
                    {
                        KeyDown(SDLK_q,q)
                        KeyDown(SDLK_w,w)
                        KeyDown(SDLK_e,e)
                        KeyDown(SDLK_a,a)
                        KeyDown(SDLK_s,s)
                        KeyDown(SDLK_d,d)
                        KeyDown(SDLK_LEFT,left)
                        KeyDown(SDLK_RIGHT,right)
                        KeyDown(SDLK_UP,up)
                        KeyDown(SDLK_DOWN,down)
                        KeyDown(SDLK_EQUALS,plus)
                        KeyDown(SDLK_MINUS,minus)
                        KeyDown(SDLK_LEFTBRACKET,leftb)
                        KeyDown(SDLK_RIGHTBRACKET,rightb)
                    }
                }
                break;
            case SDL_KEYUP:
                switch(e.key.keysym.sym)
                {
                    KeyUp(SDLK_q,q)
                    KeyUp(SDLK_w,w)
                    KeyUp(SDLK_e,e)
                    KeyUp(SDLK_a,a)
                    KeyUp(SDLK_s,s)
                    KeyUp(SDLK_d,d)
                    KeyUp(SDLK_LEFT,left)
                    KeyUp(SDLK_RIGHT,right)
                    KeyUp(SDLK_UP,up)
                    KeyUp(SDLK_DOWN,down)
                    KeyUp(SDLK_EQUALS,plus)
                    KeyUp(SDLK_MINUS,minus)
                    KeyUp(SDLK_LEFTBRACKET,leftb)
                    KeyUp(SDLK_RIGHTBRACKET,rightb)
                }
                break;
        }
    }
    return retcode;
}