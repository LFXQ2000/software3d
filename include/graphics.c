#include "graphics.h"
#include "stickhelper.h"
#include "stickmaths.h"

SDL_Window* sdlwindow;
SDL_Surface* sdlwindowsurface;

SDL_Color framebuffer[SCREENWIDTH*SCREENHEIGHT];
float zbuffer[SCREENWIDTH*SCREENHEIGHT];

SDL_Surface* framebuffersurface;

float gfov = PI/1.9f;

void InitSDL()
{
    SDL_Init(SDL_INIT_EVERYTHING);
    sdlwindow = SDL_CreateWindow("3D Renderer",SDL_WINDOWPOS_UNDEFINED,SDL_WINDOWPOS_UNDEFINED,SCREENWIDTH,SCREENHEIGHT,SDL_WINDOW_SHOWN);
    sdlwindowsurface = SDL_GetWindowSurface(sdlwindow);
    framebuffersurface = SDL_CreateRGBSurfaceFrom(framebuffer,SCREENWIDTH,SCREENHEIGHT,COLOR_32_BIT,4*SCREENWIDTH,PIXEL_MASK_Red,PIXEL_MASK_Green,PIXEL_MASK_Blue,0);
}

void ClearRender(SDL_Color c)
{
    for(int i=0;i<SCREENWIDTH*SCREENHEIGHT;i++)
    {
        memcpy(((SDL_Color*)framebuffer)+i,&c,sizeof(SDL_Color));
        zbuffer[i] = -800000.0f;
    }
}

void PresentRender()
{    
    int helpfuck = SDL_BlitSurface(framebuffersurface,NULL,sdlwindowsurface,NULL);
    if(helpfuck < 0)
    {
        printf("Error in blitting: %s",SDL_GetError());
        exit(-1);
    }
    SDL_UpdateWindowSurface(sdlwindow);
}

void PutPixel(int x, int y, SDL_Color c)
{
    if(x>=0 && x<SCREENWIDTH && y>=0 && y<SCREENHEIGHT)
        framebuffer[x+SCREENWIDTH*y] = c;
}

void PutPixelZ(int x, int y, float z, SDL_Color c)
{
    if(x>=0 && x<SCREENWIDTH && y>=0 && y<SCREENHEIGHT && zbuffer[x+SCREENWIDTH*y] < z)
    {
        framebuffer[x+SCREENWIDTH*y] = c;
        zbuffer[x+SCREENWIDTH*y] = z;
    }
}

void DrawLine(float x1, float y1, float x2, float y2, SDL_Color c)
{
    if(abs(x1-x2)>abs(y1-y2))
    {
        if(x1>x2)
        {
            SwapF(&x1,&x2);
            SwapF(&y1,&y2);
        }
        float dy = (y2-y1)/(x2-x1);
        for(float x=x1,y=y1; x < x2+1; x++,y+=dy)
            PutPixel(x,y,c);
    }
    else
    {
        if(y1>y2)
        {
            SwapF(&x1,&x2);
            SwapF(&y1,&y2);
        }
        float dx = (x2-x1)/(y2-y1);
        for(float x=x1,y=y1; y < y2+1; x+=dx,y++)
            PutPixel(x,y,c);
    }
}