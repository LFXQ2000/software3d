SOURCES := $(shell find . -name "*.c")

all:
	mkdir -p build
	mkdir -p build/models
	cp -r include/models/*.obj build/models
	gcc -Iinclude $(SOURCES) -Ofast -lm -std=c17 $$(pkg-config sdl2 --cflags --libs) -o build/software3d