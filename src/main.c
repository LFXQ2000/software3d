#include <SDL2/SDL.h>
#include <time.h>
#include "graphics.h"
#include "input.h"
#include "deltatime.h"
#include "renderer.h"
#include "mesh.h"

int main(int argc,char** argv)
{
    InitSDL();
    srand(time(NULL));
    int isRunning = 1;
    float x1,x2;
    float y;
    DeltaTimeInit();
    Vec3 rotate;
    Vec3 translate;
    translate.z = 3.0f;

    CreateSuzanne();

    MatrixPerspective((float)SCREENHEIGHT/(float)SCREENWIDTH,PI*0.6f,1000.0f,0.1f,&pmat);
    while(isRunning)
    {
        int eventCode = HandleEvents();
        if(eventCode == 1)
            isRunning = 0;
        
        ClearRender((SDL_Color){0,0,0,255});

        if(keys.q && KEY_HELD)
            rotate.z -= PI*deltaTime;
        if(keys.e && KEY_HELD)
            rotate.z += PI*deltaTime;
        if(keys.w && KEY_HELD)
            rotate.x -= PI*deltaTime;
        if(keys.s && KEY_HELD)
            rotate.x += PI*deltaTime;
        if(keys.a && KEY_HELD)
            rotate.y -= PI*deltaTime;
        if(keys.d && KEY_HELD)
            rotate.y += PI*deltaTime;
        
        if(keys.left && KEY_HELD)
            translate.x -= 2.0f*deltaTime;
        if(keys.right && KEY_HELD)
            translate.x += 2.0f*deltaTime;
        if(keys.down && KEY_HELD)
            translate.y -= 2.0f*deltaTime;
        if(keys.up && KEY_HELD)
            translate.y += 2.0f*deltaTime;
        if(keys.plus && KEY_HELD)
            translate.z -= 2.0f*deltaTime;
        if(keys.minus && KEY_HELD)
            translate.z += 2.0f*deltaTime;
        if(keys.leftb && KEY_HELD)
            gfov -= 1.0f*deltaTime;
        if(keys.rightb && KEY_HELD)
            gfov += 1.0f*deltaTime;

        Mat4 rx,ry,rz,rxy,rxyz,t,tr;

        MatrixRotateX(rotate.x,&rx);
        MatrixRotateY(rotate.y,&ry);
        MatrixRotateZ(rotate.z,&rz);
        MatrixTranslate(&translate,&t);
        MatrixMultiply(&rx,&ry,&rxy);
        MatrixMultiply(&rxy,&rz,&rxyz);
        MatrixMultiply(&rxyz,&t,&tr);

        DrawMesh(&suzanne,&tr);

        PresentRender();
        DeltaTimeUpdate();
    }

    KillSuzanne();
    return 0;
}